// Statemachine by https://github.com/mflerackers
// Type parameter or trait object: https://stackoverflow.com/a/45118378

// Generic reusable code
#[derive(Debug, PartialEq, Eq)]
pub enum Key {
    Jump,
}

// Specified code
#[derive(Debug)]
struct Data {
    x: f32,
    y: f32,
    frame: i32
}

trait State<'a, T: Sized + 'a> {
    fn key_down(&self, key: Key) -> &State<T>;
    fn update(&self, data: &mut T) -> &State<T>;
    fn draw(&self, data: &T);
}

pub struct StateMachine<'a, T: Sized + 'a> {
    data: T,
    state: &'a State<'a, T>
}

impl<'a, T: Sized> StateMachine<'a, T> {
    pub fn key_down(&mut self, key: Key) {
        //if let Some(state) = self.state.key_down(key) {
            self.state = self.state.key_down(key) ;
       // }
    }

    pub fn update(&mut self) {
        //if let Some(state) = self.state.update(&mut self.data) {
            self.state = self.state.update(&mut self.data);
        //}
    }

    pub fn draw(&self) {
        self.state.draw(&self.data);
    }
}

impl<'a> StateMachine<'a, Data> {
    fn new() -> StateMachine<'a, Data> {
        return StateMachine {
            data: Data { 
                x:0.0, 
                y:0.0, 
                frame:0 
            }, 
            state: &IdleState{}
        };
    }
}

struct IdleState;

impl<'a> State<'a, Data> for IdleState {
    fn key_down(&self, key: Key) -> &State<Data> {
        match key {
            Key::Jump => {
                return &JumpingState{};
            }
        }
    }

    fn update(&self, data: &mut Data) -> &State<Data> {
        data.frame += 1;
        return self;
    }

    fn draw(&self, data: &Data) {
        println!("Idle {:?}", data);
    }
}

struct JumpingState;

impl<'a> State<'a, Data> for JumpingState {
    fn key_down(&self, _key: Key) -> &State<Data> {
        return self;
    }

    fn update(&self, data: &mut Data) -> &State<Data> {
        data.frame += 1;
        data.y += -1.0;
        if data.y > -10.0 {
            return self;
        } else {
            return &IdleState{};
        }
    }

    fn draw(&self, data: &Data) {
        println!("Jumping {:?}", data);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn frame_count_always_updates() {
        let mut machine = StateMachine::new();
        assert!(machine.data.frame == 0i32);
        machine.update();
        assert!(machine.data.frame == 1i32);
        machine.update();
        assert!(machine.data.frame == 2i32);
    }

    #[test]
    fn state_can_be_changed() {
        let mut machine = StateMachine::new();
        machine.draw();
        assert!(machine.data.frame == 0i32);
        machine.update();
        assert!(machine.data.frame == 1i32);
        machine.draw();
        assert!(machine.data.frame == 1i32);
        machine.key_down(Key::Jump{});
        machine.update();
        assert!(machine.data.y == -1f32);
        assert!(machine.data.frame == 2i32);
        machine.draw();
        assert!(machine.data.frame == 2i32);
    }
}
